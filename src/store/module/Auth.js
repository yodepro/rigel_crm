import ApiAuth from "@/api/auth"
import _ from 'lodash'
import Cookies from 'js-cookie'

export default {
	namespaced: true,

	state: {
		user: [],
	},

	getters: {
		user: (state) => state.user,
	},

	mutations: {
		SET_USER(state, user) {
			state.user = user
		},
	},

	actions: {
		login({ commit }, params) {
			return ApiAuth.login(params).then(response => {
				Cookies.set('token', response.token)
				commit('SET_USER', response.user);

				return response

			}).catch((error) => {
				console.log(error);
				return error
			})
		},
	}
}