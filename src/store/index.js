import Vue from 'vue'
import Vuex from 'vuex'

import Auth from '@/store/module/Auth'

const store = new Vuex.Store({
  namespaced: true,
  modules: {
    Auth,
  }
});
export default store