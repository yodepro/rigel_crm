// Api users
import API from '.';
export default {
    login: (post) => {
        return API.post('/login', post, { headers: {} });
    },
    logOut: (post) => {
        return API.post('/logout')
    },
    getMe: () => {
        return API.get('/users/me')
    }

}