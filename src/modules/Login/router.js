const moduleRoute = {
    path: '/dashboard/payments/split',
    name: 'SplitPayment',
    meta: {
        icon: "pi-users",
        pageTitle: 'SplitPayment'
    },
    component: () => import('./views/SplitPayments.vue'),
}

export default (router) => {
    router.addRoute(moduleRoute);
};