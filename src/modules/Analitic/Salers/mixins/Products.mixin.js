export default {
  data() {
    return {
      filters: {
        search: {
          key: "name",
          value: null,
        },
      },
      options: {
        pages: 1,
        limit: 10,
      },
      searchByField: [
        {
          name: "Location name",
          value: "name",
        },
      ],

      columns: [

        {
          header: 'Позиция',
          field: 'code',
          frozen: true
        },
        {
          header: 'Название',
          field: 'code',
          frozen: true
        },
        {
          header: 'Рейтинг',
          field: 'code',
          frozen: false
        },
        {
          header: 'Комментарии',
          field: 'code',
          frozen: false
        },
        {
          header: 'Продажи',
          field: 'code',
          frozen: false
        },
        {
          header: 'Продажи (%)',
          field: 'code',
          frozen: false
        },
        {
          header: 'Выручка',
          field: 'code',
          frozen: false
        },
        {
          header: 'Выручка (%)',
          field: 'code',
          frozen: false
        },
        {
          header: 'Средняя цена',
          field: 'code',
          frozen: false
        },
        {
          header: 'Товары',
          field: 'code',
          frozen: false
        },
        {
          header: 'Товары с продажами',
          field: 'code',
          frozen: false
        },
        {
          header: '% товаров с продажами',
          field: 'code',
          frozen: false
        },
        {
          header: '% товаров с продажами',
          field: 'code',
          frozen: false
        },
        {
          header: 'Среднее кол-во продаж на 1 товар (с продажами)',
          field: 'code',
          frozen: false
        },
        {
          header: 'Продавцы',
          field: 'code',
          frozen: false
        },
        {
          header: 'Продавцов с продажами',
          field: 'code',
          frozen: false
        },
        {
          header: '% продавцов с продажами',
          field: 'code',
          frozen: false
        },
        {
          header: 'Средняя выручка на 1 товар',
          field: 'code',
          frozen: false
        },
        {
          header: 'Средняя выручка на 1 товар (с продажами)',
          field: 'code',
          frozen: false
        },

      ],
    }
  }
}